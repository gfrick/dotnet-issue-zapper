﻿using Domain.Entities;

namespace Application.Services
{
    public interface IUserService
    {
        User Find(string id);
        User Update(User user);
    }
}