﻿using System.Collections.Generic;
using Domain.Entities;

namespace Application.Services
{
    public interface IIssueService
    {
        IEnumerable<Issue> GetAll();
        Issue GetById(int id);
        Issue Create(Issue issue);
        Issue Update(Issue issue);
        long Delete(long id);
    }
}