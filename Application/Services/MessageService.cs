﻿using System.Collections.Generic;
using System.Linq;
using Application.Persistence;
using Domain.Entities;

namespace Application.Services
{
    public class MessageService : IMessageService
    {
        private readonly ApiContext _context;

        public MessageService(ApiContext context)
        {
            _context = context;
        }

        public IEnumerable<Message> Get()
        {
            return _context.Messages;
        }

        public IEnumerable<Message> Find(string name)
        {
            return _context.Messages.Where(message => message.Owner == name);
        }

        public Message Create(Message message)
        {
            var newMessage = _context.Messages.Add(message).Entity;
            _context.SaveChanges();
            return newMessage;
        }
    }
}