﻿using System.Collections.Generic;
using Domain.Entities;

namespace Application.Services
{
    public interface IMessageService
    {
        IEnumerable<Message> Get();
        IEnumerable<Message> Find(string name);
        Message Create(Message message);
    }
}