﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Persistence;
using Domain.Entities;

namespace Application.Services
{
    public class IssueService : IIssueService
    {
        private readonly ApiContext _context;

        public IssueService(ApiContext context)
        {
            _context = context;
        }

        public IEnumerable<Issue> GetAll()
        {
            return _context.Issues.ToList();
        }

        public Issue GetById(int id)
        {
            return _context.Issues.FirstOrDefault(t => t.Id == id);
        }

        public Issue Create(Issue issue)
        {
            if (issue == null)
            {
                throw new Exception("Cannot create null issue");
            }
            
            _context.Issues.Add(issue);
            _context.SaveChanges();
            return issue;
        }

        public Issue Update(Issue issue)
        {
            if (issue == null)
            {
                throw new Exception("Cannot update null issue");
            }

            var existing = _context.Issues.FirstOrDefault(t => t.Id == issue.Id);
            if (existing == null)
            {
                throw new Exception("Not found"); // TODO, exceptions.
            }

            existing.IsResolved = issue.IsResolved;
            existing.Title = issue.Title;
            existing.Description = issue.Description;
            existing.Priority = issue.Priority;
            existing.Type = issue.Type;

            _context.Issues.Update(existing);
            _context.SaveChanges();
            return existing;
        }

        public long Delete(long id)
        {
            var issue = _context.Issues.FirstOrDefault(t => t.Id == id);
            if (issue == null)
            {
                throw new Exception("Not Found");
            }

            _context.Issues.Remove(issue);
            _context.SaveChanges();
            return issue.Id;
        }
    }
}