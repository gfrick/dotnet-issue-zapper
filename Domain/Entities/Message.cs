﻿
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Message
    {
        [Key]
        public string Id { get; set; }
        public string Owner { get; set; }
        public string Text { get; set; }
    }
}
