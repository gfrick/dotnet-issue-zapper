﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Issue
    {
        [Key]
        public long Id { get; set; }       
        public string Owner { get; set; }
        public string Title { get; set; }
        public bool IsResolved { get; set; }
        public string Priority { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
    }
}

