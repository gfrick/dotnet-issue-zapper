﻿using System.Linq;
using Application.Persistence;
using Application.Services;
using Domain.Entities;

namespace IssueZapper.Services
{
    public class UserService : IUserService
    {
        private readonly ApiContext _context;

        public UserService(ApiContext context)
        {
            this._context = context;
        }

        public User Find(string id)
        {
            return _context.Users.FirstOrDefault(user => user.Id == id);
        }

        public User Update(User user)
        {
            _context.Users.Update(user);
            _context.SaveChanges();
            return user;
        }
    }
}