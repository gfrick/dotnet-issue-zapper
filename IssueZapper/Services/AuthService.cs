using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Application.Persistence;
using Domain.Entities;
using IssueZapper.Models;
using Microsoft.IdentityModel.Tokens;

namespace IssueZapper.Services
{
    public class AuthService : IAuthService
    {
        private readonly ApiContext _context;

        public AuthService(ApiContext context)
        {
            _context = context;
        }

        public User Register(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
            return user;
        }

        public User Login( LoginData login)
        {
            return _context.Users.SingleOrDefault(
                search => search.Email == login.Email
                       && search.Password == login.Password);
        }

        public JwtPacket CreateJwtPacket(User user)
        {
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("thisisalongersecretphrasehorsemonkey"));
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id)
            };
            var jwt = new JwtSecurityToken(claims: claims, signingCredentials: signingCredentials);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return new JwtPacket() { Token = encodedJwt, User = user };
        }
    }
}