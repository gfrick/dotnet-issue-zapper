﻿using Domain.Entities;
using IssueZapper.Models;

namespace IssueZapper.Services
{
    public interface IAuthService
    {
        User Register(User user);
        User Login( LoginData login);
        JwtPacket CreateJwtPacket(User user);
    }
}