﻿using Domain.Entities;

namespace IssueZapper.Models
{
    public class JwtPacket
    {
        public string Token { get; set; }
        public User User { get; set; }
    }
}