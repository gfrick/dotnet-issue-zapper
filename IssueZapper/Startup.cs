﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Application.Persistence;
using Application.Services;
using Domain.Entities;
using IssueZapper.Controllers;
using IssueZapper.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.OpenApi.Models;

namespace IssueZapper
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();

            services.AddDbContext<ApiContext>(opt => opt.UseInMemoryDatabase(databaseName: "InMemoryDb"));

            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IIssueService, IssueService>();
            services.AddScoped<IMessageService, MessageService>();
            services.AddScoped<IUserService, UserService>();
            
            services.AddCors(options => options.AddPolicy("Cors",
                builder => {
                    builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
                }));

            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("thisisalongersecretphrasehorsemonkey"));
            
            services.AddAuthentication(
                options => {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                }).AddJwtBearer(cfg => {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters()
                    {
                        IssuerSigningKey = signingKey,
                        ValidateAudience = false,
                        ValidateIssuer = false,
                        ValidateLifetime = false,
                        ValidateIssuerSigningKey = true
                    };
                });
                
            services.AddControllers();
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { 
                    Title = "IssueZapper API", 
                    Version = "v1",
                    Description ="Description for the API goes here.",
                    Contact = new OpenApiContact
                    {
                        Name = "George Frick",
                        Email = string.Empty,
                        Url = new Uri("https://gitlab.com/gfrick"),
                    },
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ApiContext context)
        {
            app.UseCors("Cors");

            SeedData(context);

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");

                // To serve SwaggerUI at application's root page, set the RoutePrefix property to an empty string.
                c.RoutePrefix = string.Empty;
            });
            
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute(); 
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });
        }

        public void SeedData(ApiContext context)
        {                 
            context.Users.Add(
                new User {
                    Id = "111",
                    FirstName = "George",
                    LastName = "Frick",
                    Email = "gfrick@nvisia.com",
                    Password = "p@ssword",
                    Role =  "ADMIN" 
                });

            context.Users.Add(
               new User
               {
                   Id = "222",
                   FirstName = "Jill",
                   LastName = "Reporter",
                   Email = "jreporter@nvisia.com",
                   Password = "p@ssword",
                   Role = "MANAGE_TICKET"
               });

            context.Users.Add(
              new User
              {
                  Id = "333",
                  FirstName = "Joe",
                  LastName = "Fixer",
                  Email = "jfixer@nvisia.com",
                  Password = "p@ssword",
                  Role = "BASIC" 
              });

            context.Messages.Add(
            new Message
            {
                Id = "1",
                Owner = "Jill",
                Text = "hello"
            });
            context.Messages.Add(
                new Message
                {
                    Id = "2",
                    Owner = "Joe",
                    Text = "hi"
                });

            context.Issues.Add(new Issue { Owner = "Jill", Title = "The system keeps crashing.", Type = "bug", Priority = "High", IsResolved = false });
            context.Issues.Add(new Issue { Owner = "Jill", Title = "I need a new role added.", Type = "task", Priority = "High", IsResolved = false });
            context.Issues.Add(new Issue { Owner = "Joe", Title = "Steve can no longer edit issues.", Type = "bug", Priority = "Medium", IsResolved = false });
            context.Issues.Add(new Issue { Owner = "George", Title = "I would like to be able to comment on issues.", Type = "request", Priority = "Low", IsResolved = false });
            context.SaveChanges();
        }
    }
}
