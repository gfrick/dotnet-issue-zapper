using Microsoft.AspNetCore.Mvc;
using Domain.Entities;
using IssueZapper.Models;
using IssueZapper.Services;

namespace IssueZapper.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("register")]
        public JwtPacket Register([FromBody] User user)
        {
            var registeredUser = _authService.Register(user);
            return _authService.CreateJwtPacket(registeredUser);
        }

        [HttpPost("login")]
        public ActionResult Login([FromBody] LoginData login)
        {
            var user = _authService.Login(login);

            if (user == null)
            {
                return NotFound("Email or password incorrect");
            }

            return Ok(_authService.CreateJwtPacket(user));
        }
    }
}