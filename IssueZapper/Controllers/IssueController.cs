﻿using System.Collections.Generic;
using System.Linq;
using Application.Services;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace IssueZapper.Controllers
{
    [Route("api/[controller]")]
    public class IssueController : Controller
    {
        private readonly IIssueService _issueService;
        private readonly IUserService _userService;

        public IssueController(IIssueService issueService, IUserService userService)
        {
            _issueService = issueService;
            _userService = userService;
        }

        // GET api/issue
        [HttpGet]
        public IEnumerable<Issue> GetAll()
        {
            return _issueService.GetAll();
        }

        // GET api/issue/5
        [HttpGet("{id}", Name = "GetIssue")]
        public IActionResult GetById(int id)
        {
            var issue = _issueService.GetById(id);
            if( issue == null )
            {
                return NotFound();
            }
            return new ObjectResult(issue);
        }

        // POST api/issue
        [HttpPost]
        [Authorize]
        public IActionResult Create([FromBody] Issue issue)
        {
            if( issue == null )
            {
                return BadRequest();
            }

            var foundUser = GetSecureUser();
            if (foundUser == null)
            {
                return Forbid("User not found");
            }

            issue.Owner = foundUser.FirstName;
            _issueService.Create(issue);
            return CreatedAtRoute("GetIssue", new { id = issue.Id }, issue);
        }

        // PUT api/issue/5
        [Authorize]
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Issue issue)
        {
            if (issue == null || issue.Id != id)
            {
                return BadRequest();
            }

            var existing = _issueService.GetById(id);
            if (existing == null)
            {
                return NotFound();
            }

            _issueService.Update(issue);
            return new NoContentResult();
        }

        // DELETE api/issue/5
        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var issue = _issueService.GetById(id);
            if (issue == null)
            {
                return NotFound();
            }

            _issueService.Delete(id);
            return new NoContentResult();
        }

        private User GetSecureUser()
        {
            var id = HttpContext.User.Claims.First().Value;
            return _userService.Find(id);
        }
    }
}
