﻿using System.Linq;
using Application.Services;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace IssueZapper.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{id}")]
        public ActionResult Get(string id)
        {
            var foundUser = _userService.Find(id);

            if (foundUser == null)
            {
                return NotFound("User not found");
            }

            return Ok(foundUser);
        }

        [Authorize]
        [HttpPost("me")]
        public ActionResult Post([FromBody] User user)
        {
            var foundUser = GetSecureUser();
            if (foundUser == null)
            {
                return NotFound("User not found");
            }

            foundUser.FirstName = user.FirstName ?? foundUser.FirstName;
            foundUser.LastName = user.LastName ?? foundUser.FirstName;
            foundUser.Email = user.Email ?? foundUser.Email;

            _userService.Update(foundUser);
            return Ok(foundUser);

        }

        [Authorize]
        [HttpGet("me")]
        public ActionResult Get()
        {
            var foundUser = GetSecureUser();

            if (foundUser == null)
            {
                return NotFound("User not found");
            }

            return Ok(foundUser);
        }

        private User GetSecureUser()
        {
            var id = HttpContext.User.Claims.First().Value;
            return _userService.Find(id);
        }
    }
}