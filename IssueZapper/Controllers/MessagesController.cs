﻿using System.Collections.Generic;
using System.Linq;
using Application.Services;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace IssueZapper.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class MessagesController : Controller
    {
        private readonly IMessageService _messageService;
        private readonly IUserService _userService;
        
        public MessagesController(IMessageService messageService, IUserService userService)
        {
            _messageService = messageService;
            _userService = userService;
        }

        [HttpGet()]
        public IEnumerable<Message> Get()
        {
            return _messageService.Get();
        }

        [HttpGet("{name}")]
        public IEnumerable<Message> Get(string name)
        {
            return _messageService.Find(name);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Post([FromBody] Message message)
        {
            var foundUser = GetSecureUser();

            if (foundUser == null)
            {
                return Forbid("User not found");
            }

            message.Owner = foundUser.FirstName;
            var newMessage = _messageService.Create(message);
            return Ok(newMessage);

        }

        private User GetSecureUser()
        {
            var id = HttpContext.User.Claims.First().Value;
            return _userService.Find(id);
        }
    }
}